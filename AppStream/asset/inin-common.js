// VARIABLES: HOOTSUITE
//var apiKey = '8ylpwy1x6hgco8gcwk88ssgoo3ibf3cb262'; // CIC Dev
var apiKey = '8z5l6v6ve9csw4wg4c4c8wk0k3icnag458e'; // CIC Production

// VARIABLES: ININ
var inin_messagePollInterval = 3000;
var inin_messagePollTimer;
var inin_sessionId = null;
var inin_csrfToken = null;
var inin_server = '';
var inin_pollingEnabled = false;
var inin_workgroupList = {};

var queueContentsMessageType = 'urn:inin.com:queues:queueContentsMessage';

// INITIALIZATION
function initialize() {
  console.debug('AppStream: Initialize');
  setError();

  // Hide login button
  $('#inin-loginbutton').hide();
  $('#username').html(inin_username);
  hsp.updatePlacementSubtitle(inin_username);

  // Start a polling method to get messages from the server.
  // This is being used as a keepalive as there are no subscriptions
  if (inin_messagePollTimer || inin_pollingEnabled === false) {
    return;
  }
  console.debug('AppStream: Getting messages...');
  inin_messagePollTimer = setInterval(function() {
    sendRequest('GET', inin_sessionId + "/messaging/messages", null, onMessageReceived, OnMessageError);
  }, inin_messagePollInterval);
}

// ICWS MESSAGES
function onMessageReceived(data, textStatus, jqXHR){
    var jsonData = JSON.parse(data);

    if(!jsonData || jsonData.length === 0){
        // No messages
        updateInteractionsDiv();
        return;
    }

    //TODO Review this. Check message type before processing it

    console.debug('AppStream: Processing', jsonData.length, 'messages:', jsonData);
    for(var i=0; i<jsonData.length; i++)
    {
      var currentMessage = jsonData[i];
      console.debug('AppStream: Processing Message:', currentMessage);

      switch (currentMessage.__type) {
        case queueContentsMessageType:
          if (jsonData[i].interactionsAdded) {
            console.debug('AppStream: Interactions added:', jsonData[i].interactionsAdded);
            processInteractions(jsonData[i].interactionsAdded);
          }
          if (jsonData[i].interactionsChanged) {
            console.debug('AppStream: Interactions changed:', jsonData[i].interactionsChanged);
            processInteractions(jsonData[i].interactionsChanged);
          }
          if (jsonData[i].interactionsRemoved) {
            console.debug('AppStream: Interactions removed:', jsonData[i].interactionsRemoved);
            // Attributes not available here...
            removeInteractions(jsonData[i].interactionsRemoved);
          }
          break;
        default:
          console.error('AppStream: Unhandled message type:', currentMessage.__type);
      }
      updateInteractionsDiv();
    }
}

function OnMessageError(jqXHR, textStatus, errorThrown){
    console.error('AppStream:', jqXHR);

    if (jqXHR.status == 401) {
        processUnauthorizedAccess();
    }
}

function stopMessagingTimer() {
  clearInterval(inin_messagePollTimer);
}

// WORKGROUPS
function getWorkgroups() {
    // Get all workgroups
    console.debug('AppStream: Getting workgroups...');
    if (inin_sessionId === null) {
      noSession();
    } else {
        sendRequest('GET', inin_sessionId + '/configuration/workgroups/?select=configurationId,customAttributes,hasQueue,isActive,utilizations', null, onGetWorkgroupsSuccess, onGetWorkgroupsError);
    }
}

function onGetWorkgroupsSuccess(data, textStatus, jqXHR) {
  var jsonData = JSON.parse(data);

  //console.debug('AppStream: Workgroups (raw data):', data);

  for (var workgroupIndex = 0; workgroupIndex < jsonData.items.length; workgroupIndex++) {
    var currentWorkgroup = jsonData.items[workgroupIndex];

    // Add workgroup to list if configured properly
    if (currentWorkgroup.hasQueue === true && currentWorkgroup.isActive === true && workgroupAllowsGeneric(currentWorkgroup.utilizations)) {
      if (workgroupConfiguredForReplyHootSuite(currentWorkgroup.customAttributes)) {
        // We have the reply workgroup
        console.debug('AppStream: Reply Workgroup:', JSON.stringify(currentWorkgroup));
        inin_replyWorkgroup = currentWorkgroup.configurationId.id;
        inin_workgroupList[currentWorkgroup.configurationId.id] = currentWorkgroup.configurationId.displayName;
      } else if (workgroupConfiguredForHootSuite(currentWorkgroup.customAttributes)) {
        // This workgroup is hootsuite-enabled
        console.debug('AppStream: Workgroup:', JSON.stringify(currentWorkgroup, 'is Hootsuite-enabled'));
        inin_workgroupList[currentWorkgroup.configurationId.id] = currentWorkgroup.configurationId.displayName;
      }
    }
  }

  // Get interactions from all hootsuite-enabled workgroups
  $.each(inin_workgroupList, function(key, value) {
    startWatchingInteractions(key);
  });
}

function onGetWorkgroupsError(jqXHR, textStatus, errorThrown) {
    console.error('AppStream:', jqXHR);
    setError('Error while retrieving workgroups.');
    cleanup();
}

function workgroupAllowsGeneric(utilizations) {
  if (!utilizations) return false;
  for (var utilization of utilizations) {
      // Configured for Generic Objects?
      if (utilization.mediaType == 4 && utilization.maxAssignable > 0) {
          return true;
      }
  }
  return false;
}

function workgroupConfiguredForHootSuite(customAttributes) {
  if (!customAttributes) return false;
  for (var customAttribute of customAttributes) {
    //console.debug('AppStream: Custom Attribute: name=' + customAttribute.name + ', Value=' + customAttribute.value);
    if (customAttribute.name.toLowerCase() == 'hootsuite' && customAttribute.value == 1) {
        return true;
    }
  }
  return false;
}

function workgroupConfiguredForReplyHootSuite(customAttributes) {
  if (!customAttributes) return false;
  for (var customAttribute of customAttributes) {
    //console.debug('AppStream: Custom Attribute: name=' + customAttribute.name + ', Value=' + customAttribute.value);
    if (customAttribute.name.toLowerCase() == 'hootsuite_replyworkgroup' && customAttribute.value == 1) {
        return true;
    }
  }
  return false;
}

// INTERACTIONS
function startWatchingInteractions(workgroup) {
    console.debug('AppStream: getting interactions from:', workgroup);

    // Build subscription data
    subscriptionData = {
        queueIds: [{
            queueType: 2,
            queueName: workgroup
        }],
        attributeNames: [
            "Eic_State",
            "HootSuite_RawData",
            "HootSuite_Subject",
            "HootSuite_PriorityKey",
            "HootSuite_Priority",
            "HootSuite_ReasonKey",
            "HootSuite_Reason",
            "HootSuite_WorkgroupKey",
            "HootSuite_Workgroup",
            "HootSuite_Notes",
            "HootSuite_Reply",
            "HootSuite_ReplyAgent",
            "HootSuite_ReplyWorkgroup",
            "HootSuite_Replied",
            "HootSuite_PostId",
            "HootSuite_NetworkName",
            "HootSuite_DisplayName"
        ]
    };

    sendRequest('PUT', inin_sessionId + '/messaging/subscriptions/queues/' + workgroup + 'subscription', subscriptionData, onStartWatchingInteractionsSuccess, onStartWatchingInteractionsError);
}

function onStartWatchingInteractionsSuccess(data, textStatus, jqXHR) {
    console.debug('AppStream: Successfully initiated interaction subscription:', data);
    // Interactions will be received via messages
}

function onStartWatchingInteractionsError(jqXHR, textStatus, errorThrown) {
    console.log('AppStream:', jqXHR);
    if (jqXHR.status == 405) {
        processMethodNotAllowed();
    }
}

function removeAllInteractions() {
  $("#hsstream").find("*[id*=interaction-]").each(function() {
    $(this).remove();
  });
}

function removeInteractions(interactions) {
  if (!interactions) { return; }
  for (var i = 0; i < interactions.length; i++) {
    removeInteraction(interactions[i]);
  }
}

function processInteractions(interactions) {
    if (!interactions) { return; }
    for (var i = 0; i < interactions.length; i++) {
        var interactionId = interactions[i].interactionId;
        console.debug('AppStream: Processing Interaction Id:', interactionId);
        if ($('#hsstream').find('#interaction-' + interactionId).length > 0) {
            console.debug('AppStream: found div for interaction id::', interactionId);
            updateInteraction(interactions[i]);
        } else {
            console.debug('AppStream: no div found for interaction id:', interactionId);
            if (interactions[i].attributes.hasOwnProperty('HootSuite_RawData') || interactions[i].attributes.hasOwnProperty('Hootsuite_RawData')) {
                addInteraction(interactions[i]);
            } else {
                console.debug('AppStream: interaction was removed. Not updating...');
            }
        }

    }
}

function addInteraction(interaction) {
    console.debug('AppStream: interaction added:', interaction.interactionId);
    if (!(interaction.attributes.hasOwnProperty('HootSuite_RawData') || interaction.attributes.hasOwnProperty('Hootsuite_RawData'))) {
      console.debug('interaction id', interaction.interactionId, 'is not a hootsuite interaction.');
      return;
    }

    console.log('Interaction:', interaction);
    var rawData;
    if (interaction.attributes.hasOwnProperty('HootSuite_RawData')) {
      if (interaction.attributes.HootSuite_RawData.length > 0) {
        rawData = JSON.parse(interaction.attributes.HootSuite_RawData);
      }
    } else if (interaction.attributes.hasOwnProperty('Hootsuite_RawData')) {
      if (interaction.attributes.Hootsuite_RawData.length > 0) {
        rawData = JSON.parse(interaction.attributes.Hootsuite_RawData);
      }
    }
    // No raw data? exit
    if (!rawData) return;

    var username = '';
    var body = '';
    var profilepic = '';
    var created = '';
    var createdDateTime = '';
    var interactionstate = getInteractionState(interaction);

    username = rawData.post.user.username;
    body = rawData.post.content.bodyhtml;
    created = rawData.post.datetime;
    console.debug('raw data:', rawData);
    createdDateTime = Date.parse(created);

    if (rawData.post.network.toLowerCase() == 'twitter') {
        console.debug('AppStream: This is a tweet');
        profilepic = rawData.profile.profile_image_url_https;
        // Add a new div
        console.debug('AppStream: Adding new div for interaction id', interaction.interactionId);
        jQuery('<div/>', {
            id: 'interaction-' + interaction.interactionId,
            class: "hs_message closable",
            html:   "<div class=\"close\" onClick=\"removeInteractionFromAppStream('" + interaction.interactionId + "');\"></div><div class=\"hs_message\" id=\"hs_message_" + interaction.interactionId + "\">" +
                      "<div class=\"hs_avatar\">" +
                          "<img class=\"hs_avatarImage\" src=\"" + profilepic + "\" alt=\"Avatar\" onclick=\"hsp.showUser('" + username + "')\"/>" +
                      "</div>" +
                      "<div class=\"hs_content\">" +
                          "<a href=\"#\" class=\"hs_userName\" title=\"Username\">" + username  + " (id: " + interaction.interactionId + ")</a><br>" +
                          "<a href=\"" + rawData.post.href + "\" target=\"_blank\"><span data-livestamp=\"" + created + "\"></span></a>" +
                          "<div class=\"hs_contentText\">" +
                              body +
                          "</div>" +
                      "</div>" +
                      "<div id=\"interaction-" + interaction.interactionId + "-state\" class=\"interactionstate interactionstate-" + interactionstate.toLowerCase() + "\">" + interactionstate + "</div>" +
                      "<div id=\"interaction-" + interaction.interactionId + "-reply\" class=\"reply hs_contentText\">Reply from " + interaction.attributes.HootSuite_ReplyAgent + ": " + interaction.attributes.HootSuite_Reply + "</div>" +
                      "<div id=\"interaction-reply-div-" + interaction.interactionId + "\"></div>" +
                    "</div>"
        }).appendTo('#hsstream');
    }
    else if (rawData.post.network.toLowerCase() == 'facebook') {
        console.debug('AppStream: This is a Facebook post');
        profilepic = rawData.profile.picture;
        // Add a new div
        console.debug('AppStream: Adding new div for interaction id', interaction.interactionId);
        userData = {
          "fullName": rawData.profile.name,
          "avatar": rawData.profile.picture,
          "profileUrl": rawData.profile.link,
          "extra": [
            { "label": "Gender", "value": rawData.profile.gender },
            { "label": "First Name", "value": rawData.profile.first_name },
            { "label": "Last Name", "value": rawData.profile.last_name }
          ]
        };
        jQuery('<div/>', {
            id: 'interaction-' + interaction.interactionId,
            class: "hs_message closable",
            html:   "<div class=\"close\" onClick=\"removeInteractionFromAppStream('" + interaction.interactionId + "');\"></div><div class=\"hs_message\" id=\"hs_message_" + interaction.interactionId + "\">" +
                      "<div class=\"hs_avatar\">" +
                          "<img class=\"hs_avatarImage\" src=\"" + profilepic + "\" alt=\"Avatar\" onmouseover=\"hsp.customUserInfo(userData)\"/>" +
                      "</div>" +
                      "<div class=\"hs_content\">" +
                          "<a href=\"#\" class=\"hs_userName\" title=\"Username\">" + username  + " (id: " + interaction.interactionId + ")</a><br>" +
                          "<a href=\"" + rawData.post.href + "\" target=\"_blank\"><span data-livestamp=\"" + created + "\"></span></a>" +
                          "<div class=\"hs_contentText\">" +
                              body +
                          "</div>" +
                      "</div>" +
                      "<div id=\"interaction-" + interaction.interactionId + "-state\" class=\"interactionstate interactionstate-" + interactionstate.toLowerCase() + "\">" + interactionstate + "</div>" +
                      "<div id=\"interaction-" + interaction.interactionId + "-reply\" class=\"reply hs_contentText\">Reply from " + interaction.attributes.HootSuite_ReplyAgent + ": " + interaction.attributes.HootSuite_Reply + "</div>" +
                      "<div id=\"interaction-reply-div-" + interaction.interactionId + "\"></div>" +
                    "</div>"
        }).appendTo('#hsstream');
    }

}

function updateInteraction(interaction) {
  console.debug('AppStream: interaction updated:', interaction.interactionId);

  // What has changed?
  if (interaction.attributes.hasOwnProperty('Eic_State')) {
    // Interaction state has changed
    var interactionstate = getInteractionState(interaction);
    $("#interaction-" + interaction.interactionId + "-state")
      .attr('class', 'interactionstate interactionstate-' + interactionstate.toLowerCase())
      .html(interactionstate);
  }
  if (interaction.attributes.hasOwnProperty('HootSuite_Reply') && interaction.attributes.HootSuite_Reply.length > 0) {
    // A reply has been added
    $("#interaction-" + interaction.interactionId + "-reply").html("Reply from " + interaction.attributes.HootSuite_ReplyAgent + ": " + interaction.attributes.HootSuite_Reply);
    $("#interaction-reply-div-" + interaction.interactionId).html("<button id=\"inin-tweet-reply-button\" onclick=\"replyToMessage('" + interaction.attributes.HootSuite_PostId + "', '" + interaction.attributes.HootSuite_DisplayName + "', '" + interaction.attributes.HootSuite_Reply + "')\">Insert Reply</button>");
  }
}

function removeInteraction(interactionId) {
  //Only remove if it is disconnected. If it isn't disconnected, the interaction was transferred.
  getInteractionAttribute(interactionId, 'Eic_State',
    function(data) {
      var jsonData = JSON.parse(data);
      console.log(jsonData);
      switch (jsonData.attributes.Eic_State) {
        case "I":
        case "E:":
          console.debug('interaction', interactionId, 'is disconnected. Removing.');
          removeElement('hsstream', 'interaction-' + interactionId);
          break;
        default:
          console.debug('interaction', interactionId, 'is still connected. Not removing.');
      }
    },
    function() {
      console.error('Failed to get interaction', interactionId, 'state');
    }
  );
}

function getInteractionAttribute(interactionId, attributeName, successCallback, errorCallback) {
  sendRequest('GET', inin_sessionId + '/interactions/' + interactionId + '?select=' + attributeName, null, successCallback, errorCallback);
}

function getInteractionState(interaction) {
  var interactionstate = '';
  console.debug('AppStream: Interaction state is:', interaction.attributes.Eic_State);
  switch(interaction.attributes.Eic_State) {
    case "A":
      interactionstate = "Alerting";
      $('#interaction-reply-div-' + interaction.interactionId).css('display', 'none');
      break;
    case "C":
      interactionstate = "Connected";
      $('#interaction-reply-div-' + interaction.interactionId).css('display', 'none');
      break;
    case "E":
    case "I":
      interactionstate = "Disconnected";
      $('#interaction-reply-div-' + interaction.interactionId).css('display', 'none');
      break;
    case "H":
    case "X":
      interactionstate = "Held";
      $('#interaction-reply-div-' + interaction.interactionId).css('display', 'none');
      break;
    case "O":
      if (interaction.attributes.hasOwnProperty('HootSuite_Reply') && interaction.attributes.HootSuite_Reply.length > 0) {
        interactionstate = "Ready";
        if (interaction.attributes.HootSuite_NetworkName.toLowerCase() == 'twitter') {
            $('#interaction-reply-div-' + interaction.interactionId).css('display', 'block');
        }
      } else {
        interactionstate = "Offering";
        $('#interaction-reply-div-' + interaction.interactionId).css('display', 'none');
      }
        break;
    default:
      interactionstate = 'Unknown';
      $('#interaction-reply-div-' + interaction.interactionId).css('display', 'none');
  }
  return interactionstate;
}

// DISCONNECT
function disconnectInteraction(interactionId) {
  sendRequest('POST', inin_sessionId + '/interactions/' + interactionId + '/disconnect', null, onDisconnectInteractionSuccess, onDisconnectInteractionError);
}

function onDisconnectInteractionSuccess(data, textStatus, jqXHR) {
    console.log('AppStream: Interaction disconnected:', data);
}

function onDisconnectInteractionError(jqXHR, textStatus, errorThrown) {
    console.error('AppStream: Error on disconnect interaction:', jqXHR);
    setError('Failed to disconnect interaction: ' + jqXHR.responseText);
}

// REST REQUEST
function sendRequest(verb, resource, requestData, successCallback, errorCallback) {
    // Refresh session cookie
    var sessionCookie = $.cookie(inin_sessionCookie);
    if (sessionCookie !== undefined) {
        // Set to expire in X minutes
        var expiryDate = new Date();
        expiryDate.setMinutes(expiryDate.getMinutes() + inin_sessionCookieTimeout);

        // Save cookie
        $.cookie(inin_sessionCookie, sessionCookie, { expires: expiryDate, path: '/' });
    }

    // Build request
    var request = {
        type: verb,
        success: successCallback,
        error: errorCallback,
        headers: {},
        dataFilter:dataFilter
    };

    // Enable CORS
    request.xhrFields = { withCredentials:'true' };

    // Set URL
    request.url = inin_server + '/icws/' + resource;

    // Add standard headers
    request.headers['Accept-Language'] = 'en-us';

    // Add CSRF token
    if (inin_csrfToken)
        request.headers['ININ-ICWS-CSRF-Token'] = inin_csrfToken;

    // Add data
    if (requestData)
        request.data = JSON.stringify(requestData);

    // Send request
    $.ajax(request);
}

function dataFilter(data, type) {
    /****** KNOWN ISSUE ******
     * The purpose of this is to provide content for a response that has none. ICWS
     * sends a status code of 200 and a Content-Type header of JSON even when there
     * is no content. This causes jQuery to encounter an error (Unexpected end of
     * input) that prevents it from calling any of the success, error, or complete
     * callbacks. Setting the content to an empty JSON document prevents this error.
     */
    if (data === undefined || data === '')
        return '{}';
    else
        return data;
}

// ERRORS
function processMethodNotAllowed() {
  console.error('AppStream: Got method not allowed. Try to login again.');
  // Method not allowed. This probably means the session id is null. Try to login again.
  setError('Method Not Allowed');
  loadCredentials();
  login();
}

function processUnauthorizedAccess() {
  // Unauthorized. This probably means the session id is null. Load credentials and try to login again.
  setError('Unauthorized');
  //loadCredentials();
  //login();
}

// STREAM
function removeInteractionFromAppStream(interactionId){
    if (!interactionId) { return; }

    var interactionState = $("#hsstream").find("[id=interaction-" + interactionId + "-state]");
    if (interactionState[0].textContent.length > 0) {
        if (interactionState[0].textContent == 'Disconnected') {
        } else {
            var disconnect = confirm("This will disconnect the interaction. Are you sure?");
            if (disconnect) {
                disconnectInteraction(interactionId);
            } else {
                return;
            }
        }
        removeElement('hsstream', 'interaction-' + interactionId);
    } else {
        console.debug('AppStream: Unknown interaction id:', interactionId);
    }
}

function removeElement(parentDiv, childDiv){
    if (childDiv == parentDiv) {
        return;
    }
    if (document.getElementById(childDiv)) {
        var child = document.getElementById(childDiv);
        var parent = document.getElementById(parentDiv);
        parent.removeChild(child);
    }
    else {
        return false;
    }
    updateInteractionsDiv();
}

// OTHER
function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

if (typeof String.prototype.startsWith != 'function') {
    String.prototype.startsWith = function (str){
        return this.slice(0, str.length) == str;
    };
}

if (typeof String.prototype.endsWith != 'function') {
  String.prototype.endsWith = function (str){
    return this.slice(-str.length) == str;
  };
}
