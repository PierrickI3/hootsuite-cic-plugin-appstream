// Sets an error message in the UI
function setError(message) {
    if (message == undefined || message == '') {
        $('#inin-error').css('display', 'none');
        $('#inin-error').html('No error');
    } else {
        console.error(message);
        $('#inin-error').css('display', 'block');
        $('#inin-error').html(message);
    }
}

function noSession() {
    // Show "No Session" when... there is no session (duh!)
    $('#inin-loadinginteractions').html('<div>No Session</div>');
    $('#inin-loginbutton').show();
}

// Show "Loading Interactions" spinner
function showLoadingInteractions() {
    $('#inin-loadinginteractions').show();
    $('#inin-loadinginteractions').html(
        '<div class="spinner">' +
            '<div class="bounce1"></div>' +
            '<div class="bounce2"></div>' +
            '<div class="bounce3"></div>' +
        '</div>' +
        '<div>Loading interactions...</div>');
}

function updateInteractionsDiv() {
  // Any Interactions?
  if ($("#hsstream").find("[id^=interaction-]").length > 0) {
    //console.debug('AppStream: Interactions Found');
    $('#inin-loadinginteractions').hide();
  }
  else {
    console.debug('AppStream: No Interactions Found');
    $('#inin-loadinginteractions').show();
    $('#inin-loadinginteractions').html('No Interactions Found');
  }
}
