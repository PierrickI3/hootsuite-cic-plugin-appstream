var inin_noSessionCallback;

// Loads the credentials cookie and populates fields
function loadCredentials() {
  var credsCookie = $.cookie(inin_credsCookie);
  if (credsCookie != undefined){
    // Convert to JSON object
    var credsCookieData = eval('('+credsCookie+')');
    console.debug('AppStream: Got credentials cookie');

    // Set fields
    inin_server = credsCookieData.server;
    console.debug('AppStream: server:', inin_server);
    $('#inin-server').val(inin_server);

    inin_port = credsCookieData.port;
    $('#inin-port').val(inin_port);
    console.debug('AppStream: port:', inin_port);
    inin_server += ':' + inin_port;

    inin_username = credsCookieData.username;
    $('#inin-username').val(inin_username);
    console.debug('AppStream: username:', inin_username);

    inin_password = credsCookieData.password;
    $('#inin-password').val(inin_password);
    console.debug('AppStream: password: ***********');
  } else {
    console.debug('AppStream: no creds cookie');
  }
}

// Loads the session cookie and initializes the session variables
function loadSession(noSessionCallback, startPolling) {
  // Store info
  inin_noSessionCallback = noSessionCallback;
  inin_pollingEnabled = startPolling;

  // Check cookie
  var sessionCookie = $.cookie(inin_sessionCookie);
  if (sessionCookie != undefined) {
    // Convert to JSON object
    var sessionCookieData = eval('('+sessionCookie+')');
    inin_sessionId = sessionCookieData.sessionId;
    inin_csrfToken = sessionCookieData.csrfToken;
    inin_server = sessionCookieData.server;
    inin_username = sessionCookieData.username;
    console.log('Cookie Data:', sessionCookieData);
    console.log('Username:', inin_username);

    sendRequest('GET', inin_sessionId + '/connection', null, onCheckConnectionSuccess, onCheckConnectionError);
  } else {
    console.debug('AppStream: no session cookie');

    // Notify
    if (inin_noSessionCallback) inin_noSessionCallback();
  }
}

function cleanup() {
  console.debug('AppStream: cleaning up...');
  $.removeCookie(inin_sessionCookie);
  inin_sessionId = null;
  inin_csrfToken = null;
  inin_server = null;
  inin_username = null;

  $("#username").html();
  hsp.updatePlacementSubtitle('CIC');

  removeAllInteractions();

  $('#inin-loadinginteractions').hide();
  setError();

  if (inin_noSessionCallback) inin_noSessionCallback();
}
