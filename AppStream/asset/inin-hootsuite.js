var inin_closeCustomPopupTimer;

function replyToMessage(postid, username, reply) {
    hsp.composeMessage(username + ' ' + reply, { twitterReplyToId: postid });
}

// Closes the popup window
function closeCustomPopup(apiKey) {
    // Clear the close timer if it's set
    if (inin_closeCustomPopupTimer) {
      clearTimeout(inin_closeCustomPopupTimer);
      inin_closeCustomPopupTimer = null;
    }

    // Close the popup
    console.log('AppStream: Closing custom popup...');
    hsp.closeCustomPopup(apiKey, getParameterByName('pid'));
}
