// VARIABLES: HOOTSUITE
//var apiKey = '8ylpwy1x6hgco8gcwk88ssgoo3ibf3cb262'; // CIC Dev
var apiKey = '8z5l6v6ve9csw4wg4c4c8wk0k3icnag458e'; // CIC Production

// VARIABLES: ININ
var inin_appname = "Hootsuite Integration";
var inin_replyWorkgroup = 'HootSuite-ReplyWorkgroup'; // Default value. Gets replaced after a successful login by the correct value
var inin_messagePollInterval = 10000;
var inin_messagePollTimer;
var inin_closeCustomPopupTimer;
var inin_sessionId = null;
var inin_csrfToken = null;
var inin_server = '';
var inin_username = '';
var inin_password = '';
var inin_loginRedirect;
var inin_noSessionCallback;

// INITIALIZATION
function initialize() {
    // Redirect
    if (inin_loginRedirect) {
        console.debug('Plugin: Redirecting to:', inin_loginRedirect);
        location.href = inin_loginRedirect;
        return;
    }
}

// WORKGROUPS
function getWorkgroups() {
    console.debug('Plugin: Getting workgroups...');
    sendRequest('GET', inin_sessionId + '/configuration/workgroups/?select=configurationId,customAttributes,hasQueue,isActive,utilizations', null, onGetWorkgroupsSuccess, onGetWorkgroupsError);
}

function onGetWorkgroupsSuccess(data, textStatus, jqXHR) {
    var jsonData = JSON.parse(data);

    console.debug('Plugin: Workgroups (raw data):', data);

    for (var workgroupIndex = 0; workgroupIndex < jsonData.items.length; workgroupIndex++) {
        var currentWorkgroup = jsonData.items[workgroupIndex];

        // Add workgroup to list if configured properly
        if (currentWorkgroup.configurationId.id.toLowerCase() != "hootsuite-replyworkgroup" &&
            currentWorkgroup.hasQueue == true &&
            currentWorkgroup.isActive == true &&
            workgroupAllowsGeneric(currentWorkgroup.utilizations) &&
            workgroupConfiguredForHootSuite(currentWorkgroup.customAttributes)) {
                // Add workgroup to list
                inin_workgroupList[currentWorkgroup.configurationId.id] = currentWorkgroup.configurationId.displayName;
                // Add corresponding reasons
                getWorkgroupReasons(currentWorkgroup.customAttributes);
        }
    }

    // Populate workgroup list
    $.each(inin_workgroupList, function(key, value) {
         $('#inin-workgroup')
             .append($("<option></option>")
             .attr("value",key)
             .text(value));
    });
}

function onGetWorkgroupsError(jqXHR, textStatus, errorThrown) {
    console.error('Plugin:', jqXHR);
    setError(jqXHR.statusText);
    alert('Error while retrieving workgroups.');
}

function workgroupAllowsGeneric(utilizations) {
    if (!utilizations) return false;
    for (utilization of utilizations) {
      // Configured for Generic Objects?
      if (utilization.mediaType == 4 && utilization.maxAssignable > 0) {
          return true;
      }
    }
    return false;
}

function workgroupConfiguredForHootSuite(customAttributes) {
    if(!customAttributes) return false;
    for (customAttribute of customAttributes) {
        //console.debug('Plugin: Custom Attribute: name=', customAttribute.name, ', Value=', customAttribute.value);
        if (customAttribute.name.toLowerCase() == 'hootsuite' && customAttribute.value == 1) {
            return true;
        }
    }
    return false;
}

function workgroupConfiguredForReplyHootSuite(customAttributes) {
    if (!customAttributes) return false;
    for (customAttribute of customAttributes) {
        //console.debug('Plugin: Custom Attribute: name=', customAttribute.name, ', Value=', customAttribute.value);
        if (customAttribute.name.toLowerCase() == 'hootsuite_replyworkgroup' && customAttribute.value == 1) {
            return true;
        }
    }
    return false;
}

function getWorkgroupReasons(customAttributes) {
    for (customAttribute of customAttributes) {
        //console.debug('Plugin: Custom Attribute: name=', customAttribute.name, ', Value=', customAttribute.value);
        if (customAttribute.name.toLowerCase() == 'reasons' && customAttribute.value.length > 0) {
            var reasons = customAttribute.value.split(',');
            //console.debug('Plugin: reasons:', reasons);
            $.each(reasons, function(index, value) {
                console.debug('Plugin: adding reason:', value);
                 $('#inin-reason')
                     .append($("<option />")
                     .attr("value",value)
                     .text(value));
            });
        }
    }
}

// INTERACTIONS
function createInteraction() {
    // Validate
    if ($('#inin-subject').val().trim() == '') {
        setError('Subject cannot be empty!');
        return;
    }
    if ($('textarea#inin-notes').val().trim() == '') {
        setError('Notes cannot be empty!');
        return;
    }

    // Clear errors
    setError();

    /****** KNOWN ISSUE ******
     * ICWS has a bug that prevents the attributes in additionalAttributes from being set
     * on the interaction. Because of this, it is necessary to create the interaction and
     * then set attributes on it after it has been created. It is also known that this
     * creates a race condition since the ACD handlers can fire before these attributes
     * get set. There's no way arround this until the ICWS additionalAttributes bug is fixed.
     *
     * TLDR; There's an ICWS bug with additionalAttributes and a race condition due to
     * setting attributes after the interaction has been created.
     */
    var createInteractionData = {
        "__type":"urn:inin.com:interactions:createGenericInteractionParameters",
        "queueId":{
            "queueType":2,
            "queueName":$('#inin-workgroup').val()
        },
        "initialState":5,
        "interactionDirection":1,
        "localPartyType":1,
        "remotePartyType":2,
        "remoteName":"[Hootsuite]" +
        "[" + $('#inin-priority option:selected').text() + "] " +
        $('#inin-subject').val()
    };

    // Create the interaction
    sendRequest('POST', inin_sessionId + '/interactions', createInteractionData, onCreateInteractionSuccess, onCreateInteractionError);
}

function onCreateInteractionSuccess(data, textStatus, jqXHR) {
    var jsonData = JSON.parse(data);

    console.debug('Plugin: Created interaction:', jsonData.interactionId);
    console.debug('Plugin: Reply Workgroup:', inin_replyWorkgroup);
    var setAttributeData = {
        "attributes":{
            "HootSuite_RawData":inin_rawData,
            "HootSuite_Subject":$('#inin-subject').val(),
            "HootSuite_PriorityKey":$('#inin-priority').val(),
            "HootSuite_Priority":$('#inin-priority option:selected').text(),
            "HootSuite_ReasonKey":$('#inin-reason').val(),
            "HootSuite_Reason":$('#inin-reason option:selected').text(),
            "HootSuite_WorkgroupKey":$('#inin-workgroup').val(),
            "HootSuite_Workgroup":$('#inin-workgroup option:selected').text(),
            "HootSuite_Notes":$('textarea#inin-notes').val(),
            "HootSuite_ReplyWorkgroup":inin_replyWorkgroup,
            "HootSuite_Replied": "false",
            "HootSuite_PostId": $('#inin-postid').text(),
            "HootSuite_NetworkName": $('#inin-networkname').text(),
            "HootSuite_DisplayName": $('#inin-displayname').text()
        }
    };

    // Set the attributes
    sendRequest('POST', inin_sessionId + '/interactions/' + jsonData.interactionId, setAttributeData, onSetAttributeSuccess, onSetAttributeError);
}

function onCreateInteractionError(jqXHR, textStatus, errorThrown) {
    console.error('Plugin:', jqXHR);
    setError(jqXHR.statusText);
    alert('Error creating interaction: ', jqXHR);
}

function onSetAttributeSuccess(data, textStatus, jqXHR) {
    console.debug('Plugin: attributes set');

    // Hide the create interaction content
    $('#inin-maindiv').fadeOut("500", onMainFadeOutComplete);
}

function onSetAttributeError(jqXHR, textStatus, errorThrown) {
    console.error('Plugin:', jqXHR);
    setError(jqXHR.statusText);
    alert('Error setting attributes: ', jqXHR);
}

// REST REQUEST
function sendRequest(verb, resource, requestData, successCallback, errorCallback) {
    // Build request
    var request = {
        type: verb,
        success: successCallback,
        error: errorCallback,
        headers: {},
        dataFilter:dataFilter
    };

    // Enable CORS
    request.xhrFields = { withCredentials:'true' };

    // Set URL
    request.url = inin_server + '/icws/' + resource;

    // Add standard headers
    request.headers['Accept-Language'] = 'en-us';

    // Add CSRF token
    if (inin_csrfToken) {
        request.headers['ININ-ICWS-CSRF-Token'] = inin_csrfToken;
    }

    // Add data
    if (requestData) {
        request.data = JSON.stringify(requestData);
    }

    // Send request
    console.debug('Plugin: Request:', request);
    $.ajax(request);
}

function dataFilter(data, type) {
    /****** KNOWN ISSUE ******
     * The purpose of this is to provide content for a response that has none. ICWS
     * sends a status code of 200 and a Content-Type header of JSON even when there
     * is no content. This causes jQuery to encounter an error (Unexpected end of
     * input) that prevents it from calling any of the success, error, or complete
     * callbacks. Setting the content to an empty JSON document prevents this error.
     */
    if (data == undefined || data == '')
        return '{}';
    else
        return data;
}

// OTHER
function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"), results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

if (typeof String.prototype.startsWith != 'function') {
  String.prototype.startsWith = function (str){
    return this.slice(0, str.length) == str;
  };
}

if (typeof String.prototype.endsWith != 'function') {
  String.prototype.endsWith = function (str){
    return this.slice(-str.length) == str;
  };
}
