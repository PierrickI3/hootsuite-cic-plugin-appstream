function onMainFadeOutComplete() {
    // Set success message and show
    $('#inin-maindiv').html('<div class="inin-success">Interaction Created!</div><div class="inin-success2"><button class="hs_btnCtaSml hs_btnTypeSubmit" onclick="closeCustomPopup(apiKey)">Click to close</button></div>');
    $('#inin-maindiv').fadeIn();

    inin_closeCustomPopupTimer = setTimeout(closeCustomPopup, 2000);
}

function closeCustomPopup(apiKey) {
    // Clear the close timer if it's set
    if (inin_closeCustomPopupTimer) {
        clearTimeout(inin_closeCustomPopupTimer);
        inin_closeCustomPopupTimer = null;
    }

    // Close the popup
    console.log('AppStream: Closing custom popup...');
    hsp.closeCustomPopup(apiKey, getParameterByName('pid'));
}
