function setError(message) {
    if (message == undefined || message == '') {
        $('#inin-error').css('display', 'none');
        $('#inin-error').html('No error');
    } else {
        console.error('Plugin:', message);
        $('#inin-error').css('display', 'block');
        $('#inin-error').html(message);
    }
}

function scroll() {
  if (document.getElementById('show_more')) {

    //var height = clientHeight();
    var height = document.body.offsetHeight;
    var scroll = scrollTop();

    if ((height - scroll) <= 3000 ) {
      var divId = document.getElementById('show_more');
      divId.onclick();
    }
  }
}
