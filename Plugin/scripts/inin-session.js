var inin_credsCookie = 'ININHootsuiteCredsCookie';
var inin_sessionCookie = 'ININHootsuiteSessionCookie';
var inin_sessionCookieTimeout = 3;

function login() {
  // Validate
  if ($('#inin-server').val().trim() == '') {
    setError('Server cannot be blank!');
    return;
  }
  if ($('#inin-port').val().trim() != '' && !isNumeric($('#inin-port').val().trim())) {
    setError('Port must be a number!');
    return;
  }
  if ($('#inin-username').val().trim() == '') {
    setError('Username cannot be blank!');
    return;
  }
  if ($('#inin-password').val() == '') {
    setError('Password cannot be blank!');
    return;
  }

  // Clear error message
  setError();

  // Set local vars
  inin_server = $('#inin-server').val();
  if (inin_server.endsWith('/')) {
    // Remove trailing slash
    inin_server = inin_server.substring(0, inin_server.length - 1);
  }
  if ($('#inin-port').val().trim() == '') {
    // Add default ports if none specified
    if (inin_server.startsWith('https'))
        inin_server += ':8019';
    else
        inin_server += ':8018';
  } else {
    // Add specified port
    inin_server += ':' + $('#inin-port').val().trim();
  }
  inin_username = $('#inin-username').val().trim();
  inin_password = $('#inin-password').val();

  // Save credentials cookie
  var cookieData = '{ '+
    '"server":"' + $('#inin-server').val().trim() + '", ' +
    '"port":"' + $('#inin-port').val().trim() + '", ' +
    '"username":"' + $('#inin-username').val().trim() + '", ' +
    '"password":"' + $('#inin-password').val() + '"' +
    ' }';
  $.cookie(inin_credsCookie, cookieData, { path: '/' });

  // Build auth request
  loginData = {
    "__type":"urn:inin.com:connection:icAuthConnectionRequestSettings",
    "applicationName":inin_appname,
    "userID":inin_username,
    "password":inin_password
  };

  // Log in
  sendRequest("POST","connection", loginData, onLoginSuccess, onLoginError);
}

function onLoginSuccess(data, textStatus, jqXHR) {
    var jsonData = JSON.parse(data);

    console.debug('Plugin: Login response data (raw data):', data);

    // Set session cookie
    var cookieData = '{ '+
        '"sessionId":"' + jsonData.sessionId + '", ' +
        '"csrfToken":"' + jsonData.csrfToken + '", ' +
        '"server":"' + inin_server + '"' +
        ' }';
    $.cookie(inin_sessionCookie, cookieData, { path: '/' });

    inin_sessionId = jsonData.sessionId;
    inin_csrfToken = jsonData.csrfToken;

    initialize();
}

function onLoginError(jqXHR, textStatus, errorThrown) {
    console.error(jqXHR);

    if (jqXHR.status == 501) {
        setError('SSL certificate is not trusted. Please contact your system administrator.');
    } else {
        setError(jqXHR.statusText + ': Please check your credentials');
    }
}

function onCheckConnectionSuccess(data, textStatus, jqXHR) {
  var jsonData = JSON.parse(data);

  if (jsonData.connectionState == 1) {
    console.debug('Plugin: Connection is up');
    initialize();
  } else {
    console.debug('Plugin: Connection is unavailable');

    cleanup();

    // Notify
    if (inin_noSessionCallback) {
      inin_noSessionCallback();
    }
  }
}

function onCheckConnectionError(jqXHR, textStatus, errorThrown) {
  console.error('Plugin:', jqXHR);

  cleanup();

  // Notify
  if (inin_noSessionCallback) {
    inin_noSessionCallback();
  }
}

function logout() {
    console.debug('Plugin: Disconnecting session...');
    sendRequest("DELETE", inin_sessionId + "/connection", null, onLogoutSuccess, onLogoutError);
}

function onLogoutSuccess(data, textStatus, jqXHR) {
    var jsonData = JSON.parse(data);

    clearInterval(inin_messagePollTimer);
    $.removeCookie(inin_sessionCookie);
}

function onLogoutError(jqXHR, textStatus, errorThrown) {
    console.error('Plugin:', jqXHR);
    setError(jqXHR.statusText);
}
