function cleanup() {
  $.removeCookie(inin_sessionCookie);
  inin_sessionId = null;
  inin_csrfToken = null;
  inin_server = null;
}

function loadCredsCookie() {
    var credsCookie = $.cookie(inin_credsCookie);
    if (credsCookie != undefined){
        // Convert to JSON object
        var credsCookieData = eval('('+credsCookie+')');
        console.debug('Plugin: Got credentials cookie');

        // Set fields
        $('#inin-server').val(credsCookieData.server);
        $('#inin-port').val(credsCookieData.port);
        $('#inin-username').val(credsCookieData.username);
        $('#inin-password').val(credsCookieData.password);
    } else {
        console.debug('Plugin: no creds cookie');
    }
}

function loadSessionCookie(noSessionCallback) {

    inin_noSessionCallback = noSessionCallback;

    // Check cookie
    var sessionCookie = $.cookie(inin_sessionCookie);
    if (sessionCookie != undefined) {
        // Convert to JSON object
        var sessionCookieData = eval('('+sessionCookie+')');
        console.debug('Plugin: Got session cookie');

        inin_sessionId = sessionCookieData.sessionId;
        inin_csrfToken = sessionCookieData.csrfToken;
        inin_server = sessionCookieData.server;

        sendRequest('GET', inin_sessionId + '/connection', null, onCheckConnectionSuccess, onCheckConnectionError);
    } else {
        console.debug('Plugin: no session cookie');

        if (inin_noSessionCallback) {
          inin_noSessionCallback();
        }
    }
}
