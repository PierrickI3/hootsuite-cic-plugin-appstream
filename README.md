# Hootsuite/CIC Integration #

Welcome to the Hootsuite Plugin for the integration between Hootsuite and CIC from Interactive Intelligence

### What is this repository for? ###

* Creates a view and an AppStream to send requests to CIC
* Monitors CIC workgroups for agent replies

### How do I get set up? ###

* Apply for a Hootsuite developer account
* Create an app and add a view and an appstream
* Set up a web server with Apache using https (required) on port 80 (yes, I know but that's due to a Hootsuite bug)
* Clone this repository
* The plugin will show in Hootsuite under the arrow on the top-right of a social network entry

### Contribution guidelines ###

* Thanks to Tim Smith for creating the basic integration. I created one previously but it was far more complex and was using IceLib
* The ICWS team in Interactive Intelligence
* Actually, everyone at Interactive Intelligence!

### Who do I talk to? ###

* Pierrick Lozach, Principal Solutions Engineer at Interactive Intelligence